#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: facsub.alg		*/
/* Compiled at Thu Jul 12 12:49:50 2018		*/


#include	<stdio.h>
#include "facsub.h"

//	Code for the global declarations

 /* factorial declared at line 3*/
int _factorial_41 (char	*Ln, int (*An)( char *, int), int (*Vn)(char *, int)){ 
 struct ___factorial_41_rec local_data_factorial;
struct ___factorial_41_rec *LP = & local_data_factorial;
LP -> Ln = Ln;
LP -> An = An;
LP -> Vn = Vn;

{ // code for block at line 5
 (LP ) -> __res_val =  ( (((LP) -> Vn)(((LP) -> Ln), 0)) < (1)  )? 1 : (_factorial_41 (LP, A_jff_0A, _jff_0A)) * (((LP) -> Vn)(((LP) -> Ln), 0));
outinteger (1, _factorial_41 (LP, A_jff_1A, _jff_1A));
}
return LP -> __res_val;

}
int  A_jff_0A (char *LP, int V){
fault (" no assignable object",6);
}
int  _jff_0A (char *LP, int d){
return (((((struct ___factorial_41_rec *)(LP))) -> Vn)(((((struct ___factorial_41_rec *)(LP))) -> Ln), 0)) - (1);
}
int  A_jff_1A (char *LP, int V){
fault (" no assignable object",7);
}
int  _jff_1A (char *LP, int d){
return 5;
}
int _i_40; /* i declared at line 11*/
int  A_jff_2A (char *LP, int V){
fault (" no assignable object",14);
}
int  _jff_2A (char *LP, int d){
return 5;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 3
_i_40=0;
_i_40=(_i_40) + (1);
 while ((int)((_i_40) <= (10))) {
{ // code for block at line 0
outinteger (1, _factorial_41 (LP, A_jff_2A, _jff_2A));
}
_i_40=(_i_40) + (1); }
}
}
