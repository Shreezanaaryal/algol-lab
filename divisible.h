#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: divisible.alg		*/
/* Compiled at Thu Jul 12 13:25:10 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void newline (int); /* newline declared at line 24*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _i_40; /* i declared at line 2*/
extern int _count_40; /* count declared at line 3*/
