#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: divisible.alg		*/
/* Compiled at Thu Jul 12 13:25:10 2018		*/


#include	<stdio.h>
#include "divisible.h"

//	Code for the global declarations

int _i_40; /* i declared at line 2*/
int _count_40; /* count declared at line 3*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_count_40=0;
for (_i_40=1; ( _i_40- (50)) * sign ((double)1 )<= 0;_i_40 +=1) if ((((_i_40) / (2)) * (2)) == (_i_40))
 _count_40=(_count_40) + (1);
outstring (1, "This many numbers between 1 and 50 are divisible by 2");
outinteger (1, _count_40);
newline (1);
}
}
