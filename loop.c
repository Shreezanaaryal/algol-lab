#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: loop.alg		*/
/* Compiled at Thu Jul 12 12:43:23 2018		*/


#include	<stdio.h>
#include "loop.h"

//	Code for the global declarations

int _a_40[10 - 1 +1]; /* a declared at line 2*/
int __dv0 [2 * 2 + DOPE_BASE];
int	*_b_40; /* b declared at line 3*/
double _c_40[0 -  -(10) +1]; /* c declared at line 5*/
int _i_40; /* i declared at line 6*/


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
for (_i_40=1; ( _i_40- (10)) * sign ((double)1 )<= 0;_i_40 +=1){ _a_40 [_i_40-1]=(_i_40) * (_i_40);
outinteger (1, _a_40 [_i_40-1]);
}
newline (1);
_i_40=1;
_i_40=(_i_40) + (1);
 while ((int)((_i_40) <= (10))) {{ _a_40 [_i_40-1]=((_i_40) * (_i_40)) * (_i_40);
outinteger (1, _a_40 [_i_40-1]);
}
_i_40=(_i_40) + (1); }
}
}
